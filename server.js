//import fastify & mongoose
const fastify = require('fastify');
const mongoose = require('mongoose');
const dotenv = require("dotenv");
const mediaRoute = require('./routes/media.routes');
const contentRangeHook = require('./hooks/contentRangeHook');

const app = fastify();
dotenv.config();

//connected fastify to mongoose
try {
  mongoose.connect(process.env.CONNECT_DB);
} catch (e) {
  console.error(e);
}

app.addHook('preHandler', contentRangeHook);
mediaRoute(app);

//handle root route
app.get('/', (request, reply) => {
  try{
    reply.send("Hello world!");
  } catch(e) {
    console.error(e);
  }
})

//set application listening on port 8080 of localhost
app.listen(8080, '0.0.0.0', (err, address) => {
  if (err) {
    console.error(err);
    process.exit(1);
  }
  console.log(`Server running on ${address}`);
});
