const Media = require('../models/media.model');

module.exports = {
    // # create a media
    addMedia: async (request, reply) => {
        try {
            const media = request.body;
            const newMedia = await Note.create(media);
            reply.code(201).send(newMedia);
        } catch (e) {
            reply.code(500).send(e);
        }
    },

    // #get the list of media
    getMediaList: async (request, reply) => {
        try {
            const medias = await Media.find({});
            reply.code(200).send(medias);
        } catch (e) {
            reply.code(500).send(e);
        }
    },

    // #get a single media
    getAMedia: async (request, reply) => {
        try {
            const mediaId = request.params.id;
            const media = await Media.findById(mediaId);
            reply.code(200).send(media);
        } catch (e) {
            reply.code(500).send(e);
        }
    },

    // #update a media
    updateAMedia: async (request, reply) => {
        try {
            const mediaId = request.params.id;
            const updates = request.body;
            await Media.findByIdAndUpdate(mediaId, updates);
            const mediaToUpdate = await Media.findById(mediaId);
            reply.code(200).send({data: mediaToUpdate});
        } catch (e) {
            reply.code(500).send(e);
        }
    },

    // #delete a media
    deleteAMedia: async (request, reply) => {
        try {
            const mediaId = request.params.id;
            const mediaToDelete = await Media.findById(mediaId);
            await Media.findByIdAndDelete(noteId);
            reply.code(200).send({data: mediaToDelete});
        } catch (e) {
            reply.code(500).send(e);
        }
    }
};
