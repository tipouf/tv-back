#!/bin/sh
# date: 2018-04-13
# author: Clément Désiles <clement.desiles@hoppen.care>
# description: creates a docker image with builded package
set -e
cd "$(dirname "$0")"

VERSION="$(git describe --always | sed 's/^v//')"
PACKAGE="$(git config --get remote.origin.url | awk -F '@[^:/]+[:/]?' '{print $(NF)}' | sed s'/.git$//' | sed s'/telecomsante\///')"
PROJECT=$(basename $(git config --get remote.origin.url) .git)

DOCKER_TAG=${DOCKER_TAG:-$VERSION}
DOCKER_REGISTRY=${DOCKER_REGISTRY:=artifacts.hoppen.care}
DOCKER_IMAGE=${DOCKER_IMAGE:=$DOCKER_REGISTRY/docker/$PACKAGE:$DOCKER_TAG}
DOCKERFILE=${DOCKERFILE:-"Dockerfile"}
SECURITY=${SECURITY:-"false"}
DOCKER_STAGE=${DOCKER_STAGE:-""}
ARCH=${ARCH:-"x64"}

# ssh key for gitlab.com, needed for grunt
DEPLOY_KEY_FILE=${DEPLOY_KEY_FILE:=~/.ssh/id_rsa}
# get the key content in a variable
DEPLOY_KEY=$(sed -E ':a;N;$!ba;s/\r{0,1}\n/\\n/g' $DEPLOY_KEY_FILE)

# package labels
COMMIT_SHA=`git rev-parse --short HEAD`

print_help () {
  echo "Usage: ${SCRIPTNAME} [--push] [--sec] [-h|--help]\nDocker image factory for $PACKAGE

  Available parameters:

    --ia32              Make a 32 bit compatible image
    --tarball           Build a tarball out of the docker image
    --push              Push docker image to the registry \$DOCKER_REGISTRY
    --sec               Obfuscate the javascript code (not used)
    --build-env         Build build-env stage only
    --save-cache        Build and push a cache image (usually the build-env stage)
    --get-image         Prints the docker image name
    --get-cache-image   Prints the docker cache image name
    --help              Print this help message

  Available variables:

    ARCH          target architecture (x64 or ia32)
    DOCKER_STAGE  multi-stage build to stop to
    DOCKER_TAG    the docker end tag (ex: latest)
    DOCKERFILE    path of the Dockerfile to use
    SECURITY      if set to true, add an extra build-arg
    "
  exit 0
}

get_image () {
  echo "${DOCKER_IMAGE}"
}

get_cache_image () {
  echo "${DOCKER_REGISTRY}/${PACKAGE}:build-cache-${ARCH}"
}

get_develop_image () {
  echo "${DOCKER_REGISTRY}/${PACKAGE}:develop"
}

build_cache_image () {
  docker build \
    --target ${DOCKER_STAGE:-"build-env"} \
    --pull \
    --label "commit-sha=$COMMIT_SHA" \
    --label "version=$VERSION" \
    --cache-from "$(get_cache_image)" \
    --build-arg SSH_PRIVATE_KEY="$DEPLOY_KEY" \
    --build-arg SECURITY="$SECURITY" \
    --build-arg ARCH="$ARCH" \
    --tag "$(get_cache_image)" \
    -f $DOCKERFILE \
    .
  docker push $(get_cache_image)
  echo "Docker image pushed: $(get_cache_image)\n"
}

build_image () {

  # fill the cache if any
  docker pull $(get_cache_image) 2> /dev/null || true
  docker pull $(get_develop_image) 2> /dev/null || true

  # build-cache image to speed up builds
  [ ! -z "$SAVE_CACHE" ] && build_cache_image || true

  # stop at the build stage if required
  [ ! -z "$DOCKER_STAGE" ] && TARGET="--target $DOCKER_STAGE" || true

  # multi-stage build made easy
  docker build \
    $TARGET \
    --pull \
    --label "commit-sha=$COMMIT_SHA" \
    --label "version=$VERSION" \
    --cache-from "$(get_cache_image)" \
    --cache-from "$(get_develop_image)" \
    --build-arg SSH_PRIVATE_KEY="$DEPLOY_KEY" \
    --build-arg SECURITY="$SECURITY" \
    --build-arg ARCH="$ARCH" \
    --tag "$DOCKER_IMAGE" \
    -f $DOCKERFILE \
    .

  echo "Docker image successfully built:
       Dockerfile recipe: $DOCKERFILE
       Package: $PACKAGE
       Version: $DOCKER_TAG
       Registry: $DOCKER_REGISTRY
       Arch: $ARCH"
}

push_image () {
  docker push $DOCKER_IMAGE
  echo "Docker image pushed: $DOCKER_IMAGE"
}

build_tarball () {
  mkdir -p ./dist
  CONTAINER_NAME=$(echo ${PACKAGE}-${DOCKER_TAG}-$(cat /proc/sys/kernel/random/uuid) | sed 's/\//-/g')
  ARCHIVE=${PROJECT}-${DOCKER_TAG}.tar.gz
  CHANGELOG=$(sed -e '1,/##/d' -e '/##/,$d' CHANGELOG.md | sed -e ':a;N;$!ba;s/\n/\\n/g' | sed -e 's/\//\\\//g')
  echo $CHANGELOG
  DEB_ARCH="amd64"
  if [ "$ARCH" = "ia32" ]; then
    DEB_ARCH="i386"
  fi
  docker run --name $CONTAINER_NAME \
             --user root \
             --entrypoint sh \
             $DOCKER_IMAGE \
             -c "echo $DOCKER_TAG > /app/VERSION; \
                 find /app/deb/ -type f -exec sed -i \"s/__VERSION__/${VERSION}/g;s/__ARCH__/${DEB_ARCH}/g;s/__CHANGELOG__/${CHANGELOG}/g\" {} +; \
                 tar --transform \"s/^app/${PROJECT}-${DOCKER_TAG}/\" -czvf /$ARCHIVE /app;"
  docker cp $CONTAINER_NAME:/$ARCHIVE ./dist/
  docker rm -f $CONTAINER_NAME
  echo "Tarball successfully built:
       File: ./dist/$ARCHIVE
       Size: `ls -l ./dist/$ARCHIVE | awk '{print $5}'`"
}

#
# MAIN
#
while test -n "$1"; do
  case "$1" in
    --tarball)
      TARBALL_ENABLED="true" ;;
    --ia32|--i686)
      ARCH="ia32" ;;
    -p|--push)
      PUSH_ENABLED="true" ;;
    --sec)
      SECURITY="true" ;;
    --build-env)
      DOCKER_STAGE="build-env" ;;
    --save-cache)
      SAVE_CACHE="true" ;;
    --get-cache-image)
      get_cache_image && exit 0 ;;
    --get-image)
      get_image && exit 0 ;;
    -h|--help)
      print_help ;;
  esac
  shift
done

# Update tags for ia32 arch and obfuscated source code
if [ "$ARCH" = "ia32" ]; then DOCKER_TAG="${DOCKER_TAG}-ia32"; fi
if [ "$SECURITY" = "true" ]; then DOCKER_TAG="${DOCKER_TAG}-sec"; fi

build_image
[ -n "$TARBALL_ENABLED" ] && build_tarball
[ -n "$PUSH_ENABLED" ] && push_image

exit 0
