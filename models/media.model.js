var mongoose = require('mongoose');
var Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

const mediaSchema = new Schema({
    id: {type:ObjectId, ref:'Event'},
    name: {type: String, required: true}, // String is shorthand for {type: String}
    url: {type: String, required: true},
    type: {type: String, required: true}, // TV, RADIO, MEDIA
    order: {type: Number, required: true},
    tags: [String],
    categories: [String],
    lang: {type:String, required: true},
    fav: {type: Boolean, required: true, default: false},
    img: {
        data: Buffer,
        contentType: String
    }
});

const Media = mongoose.model('media', mediaSchema);

module.exports = Media;
