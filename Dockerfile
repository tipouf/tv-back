# ----- build image -----
FROM node:17.3.0 AS build-env

RUN apt-get update
RUN apt-get install -y git openssh-client gettext git


ENV npm_config_unsafe_perm=true

WORKDIR /build

COPY . /build
RUN npm i -g typescript
RUN npm ci && npm run build && npm prune --production

# ----- production image -----
FROM node:17.3.0-alpine

WORKDIR /app
LABEL maintainer="Nicolas Cindon <nicolas.cindon@hoppen.care>"
COPY --from=build-env /build/dist /app/dist
COPY --from=build-env /build/node_modules /app/node_modules
COPY --from=build-env /build/package.json /app
COPY --from=build-env /build/package-lock.json /app
COPY --from=build-env /build/migration /app/migration

RUN apk add --update --no-cache curl

HEALTHCHECK CMD curl --fail http://localhost:8080/api/healthcheck || exit 1
EXPOSE 8080

ENTRYPOINT [ "npm" ]
CMD [ "start" ]
