const Media = require('../models/media.model');

const {addMedia, getAMedia, getMediaList, deleteAMedia, updateAMedia} = require("../controllers/media.controller");

async function routes(app, options) {
    app.get("/api/media", getMediaList);        // get media list
    app.post("/api/media", addMedia);           // add a media
    app.put("/api/media/:id", updateAMedia);    // edit a media
    app.get("/api/media/:id", getAMedia);       // get a media
    app.post("/api/media/:id", deleteAMedia);   // delete a media
}
module.exports = routes;
